package utils

import (
	"log"
	"runtime"
)

func LogError(err error) {
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		log.Printf("Error: %s\nOccurred in file: %s on line: %d", err, file, line)
	}
}
