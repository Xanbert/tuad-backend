# API

#### login

##### 调用方式

url=`/login`

##### 输入

| Key      | Value 类型 | Value 意义 |
| -------- | ---------- | ---------- |
| username | string     | 用户名     |
| password | string     | 密码       |

##### 输出

| Key    | Value 类型 | Value 意义 |
| ------ | ---------- | ---------- |
| userid | int        | 用户ID     |
| class  | int        | 用户类型   |

##### 异常处理

UB for now

#### register

##### 调用方式

url=`/register`

##### 输入

| Key      | Value 类型 | Value 意义 |
| -------- | ---------- | ---------- |
| username | string     | 用户名     |
| password | string     | 密码       |
| class    | int        | 用户类型   |
| sex      | int        | 性别       |
| age      | int        | 年龄       |
| region   | string     | 地区       |

##### 输出

| Key    | Value 类型 | Value 意义 |
| ------ | ---------- | ---------- |
| userid | int        | 用户 ID    |
| class  | int        | 用户类型   |

其实这里返回个success或者啥都不管也就行？先抄一遍登录吧

##### 异常处理

UB for now

#### userDel

##### 调用方式

url=`/userdel`

##### 输入

| Key    | Value 类型 | Value 意义 |
| ------ | ---------- | ---------- |
| userid | int        | 用户 ID    |

> [!CAUTION]
>
> 有变动，鉴于其他的用户操作都使用userid，我认为这里保持一致会比较好

##### 输出

| Key  | Value 类型 | Value 意义 |
| ---- | ---------- | ---------- |
| -    | -          | -          |

##### 异常处理

UB for now

#### userList

##### 调用方式

url=`/userlist`

##### 输入

| Key  | Value 类型 | Value 意义 |
| ---- | ---------- | ---------- |
| -    | -          | -          |

##### 输出

> [!NOTE]
>
> 数组形式返回

| Key      | Value 类型 | Value 意义 |
| -------- | ---------- | ---------- |
| username | string     | 用户名     |
| password | string     | 密码       |
| class    | int        | 用户类型   |
| sex      | int        | 性别       |
| age      | int        | 年龄       |
| region   | string     | 地区       |

> 干啥的？

##### 异常处理

UB for now

#### tutorList

##### 调用方式

url=`/tutorlist`

##### 输入

| Key            | Value 类型 | Value 意义 |
| -------------- | ---------- | ---------- |
| subject        | int        | 科目       |
| sex            | int        | 性别       |
| region         | string     | 地区       |
| educationstage | int        | 学段       |
| lowrate        | real       | 评分下限   |
| lowprice       | int        | 价格下限   |
| highprice      | int        | 价格上限   |

##### 输出

> [!NOTE]
>
> 数组形式返回

| Key      | Value 类型 | Value 意义 |
| -------- | ---------- | ---------- |
| userid   | int        | 用户 ID    |
| username | string     | 用户名     |
| sex      | int        | 性别       |
| age      | int        | 年龄       |
| region   | string     | 地区       |
| subject  | int        | 科目       |
| expprice | int        | 预期报酬   |
| rate     | real       | 评分       |
| info     | string     | 备注       |

##### 异常处理

UB for now

#### requirementAdd

##### 调用方式

url=`/requirementadd`

##### 输入

| Key            | Value 类型 | Value 意义 |
| -------------- | ---------- | ---------- |
| userid         | int        | 用户 ID    |
| subject        | int        | 科目       |
| educationstage | int        | 学段       |
| info           | string     | 备注       |

##### 输出

| Key  | Value 类型 | Value 意义 |
| ---- | ---------- | ---------- |
| -    | -          | -          |

##### 异常处理

UB for now

#### tutorAdd

##### 调用方式

url=`/tutoradd`

##### 输入

| Key            | Value 类型 | Value 意义 |
| -------------- | ---------- | ---------- |
| userid         | int        | 用户 ID    |
| subject        | int        | 科目       |
| educationstage | int        | 学段       |
| expprice       | int        | 预期报酬   |
| info           | string     | 备注       |

##### 输出

| Key  | Value 类型 | Value 意义 |
| ---- | ---------- | ---------- |
| -    | -          | -          |

##### 异常处理

UB for now

#### userUpdate

##### 调用方式

url=`/userupdate`

##### 输入

| Key      | Value 类型 | Value 意义 |
| -------- | ---------- | ---------- |
| userid   | int        | 用户 ID    |
| username | string     | 用户名     |
| password | string     | 密码       |
| class    | int        | 用户类型   |
| sex      | int        | 性别       |
| age      | int        | 年龄       |
| region   | string     | 地区       |

##### 输出

| Key    | Value 类型 | Value 意义 |
| ------ | ---------- | ---------- |
| userid | int        | 用户 ID    |
| class  | int        | 用户类型   |

##### 异常处理

UB for now

#### rateAdd

##### 调用方式

url=`/rateadd`

##### 输入

| Key     | Value 类型 | Value 意义   |
| ------- | ---------- | ------------ |
| userid  | int        | 评价者 ID    |
| rateeid | int        | 被评价者 ID  |
| rate    | double     | 评分，0 - 10 |
| info    | string     | 评价信息     |

##### 输出

| Key  | Value 类型 | Value 意义 |
| ---- | ---------- | ---------- |
| -    | -          | -          |

##### 异常处理

UB for now

#### rateUpdate

##### 调用方式

url=`/rateupdate`

##### 输入

| Key     | Value 类型 | Value 意义   |
| ------- | ---------- | ------------ |
| userid  | int        | 评价者 ID    |
| rateeid | int        | 被评价者 ID  |
| rate    | double     | 评分，0 - 10 |
| info    | string     | 评价信息     |

##### 输出

| Key  | Value 类型 | Value 意义 |
| ---- | ---------- | ---------- |
| -    | -          | -          |

##### 异常处理

UB for now

#### rateDel

##### 调用方式

url=`/ratedel`

##### 输入

| Key     | Value 类型 | Value 意义  |
| ------- | ---------- | ----------- |
| userid  | int        | 评价者 ID   |
| rateeid | int        | 被评价者 ID |

##### 输出

| Key  | Value 类型 | Value 意义 |
| ---- | ---------- | ---------- |
| -    | -          | -          |

##### 异常处理

UB for now

#### tutorRate

##### 调用方式

url=`/tutorrate`

##### 输入

| Key    | Value 类型 | Value 意义            |
| ------ | ---------- | --------------------- |
| userid | int        | 需要查询评价的用户 ID |

##### 输出

> [!NOTE]
>
> 数组形式返回

| Key    | Value 类型 | Value 意义 |
| ------ | ---------- | ---------- |
| userid | int        | 评价者 ID  |
| rate   | double     | 评分       |
| info   | string     | 评价信息   |

##### 异常处理

UB for now

#### userRate

##### 调用方式

url=`/userrate`

##### 输入

| Key    | Value 类型 | Value 意义 |
| ------ | ---------- | ---------- |
| userid | int        | 评价者 ID  |

##### 输出

> [!NOTE]
>
> 数组形式返回

| Key    | Value 类型 | Value 意义  |
| ------ | ---------- | ----------- |
| userid | int        | 被评价者 ID |
| rate   | double     | 评分        |
| info   | string     | 评价信息    |

##### 异常处理

UB for now

#### userInfo

##### 调用方式

url=`/userinfo`

##### 输入

| Key    | Value 类型 | Value 意义 |
| ------ | ---------- | ---------- |
| userid | int        | 用户 ID    |

##### 输出

| Key      | Value 类型 | Value 意义 |
| -------- | ---------- | ---------- |
| username | string     | 用户名     |
| class    | int        | 用户类型   |
| sex      | int        | 性别       |
| age      | int        | 年龄       |
| region   | string     | 地区       |

##### 异常处理

UB for now

#### requirementInfo

##### 调用方式

url=`/requirementinfo`

##### 输入

| Key    | Value 类型 | Value 意义 |
| ------ | ---------- | ---------- |
| userid | int        | 用户 ID    |

##### 输出

> [!NOTE]
>
> 数组形式返回

| Key            | Value 类型 | Value 意义 |
| -------------- | ---------- | ---------- |
| subject        | int        | 科目       |
| educationstage | int        | 学段       |
| info           | string     | 备注       |

##### 异常处理

UB for now

#### tutorInfo

##### 调用方式

url=`/tutorinfo`

##### 输入

| Key    | Value 类型 | Value 意义 |
| ------ | ---------- | ---------- |
| userid | int        | 用户 ID    |

##### 输出

> [!CAUTION]
>
> 每一项的rate都是相同的，也就是一个user的评分若有则在哪里取都是相同的，懒得为每个学段、科目维护单独的评分了（因为触发器写起来太恶心了，报错还不明不白的），也懒得改原来的表了，就这么的吧，锅甩给前端（

> [!NOTE]
>
> 数组形式返回

| Key            | Value 类型 | Value 意义 |
| -------------- | ---------- | ---------- |
| subject        | int        | 科目       |
| educationstage | int        | 学段       |
| expprice       | int        | 预期报酬   |
| rate           | double     | 评分       |
| info           | string     | 备注       |

##### 异常处理

UB for now

#### requirementUpdate

##### 调用方式

url=`/requirementupdate`

##### 输入

| Key            | Value 类型 | Value 意义 |
| -------------- | ---------- | ---------- |
| userid         | int        | 用户 ID    |
| subject        | int        | 科目       |
| educationstage | int        | 学段       |
| info           | string     | 备注       |

##### 输出

| Key  | Value 类型 | Value 意义 |
| ---- | ---------- | ---------- |
| -    | -          | -          |

##### 异常处理

UB for now

#### requirementDelete

##### 调用方式

url=`/requirementdelete`

##### 输入

| Key            | Value 类型 | Value 意义 |
| -------------- | ---------- | ---------- |
| userid         | int        | 用户 ID    |
| subject        | int        | 科目       |
| educationstage | int        | 学段       |

##### 输出

| Key  | Value 类型 | Value 意义 |
| ---- | ---------- | ---------- |
| -    | -          | -          |

##### 异常处理

UB for now

#### tutorUpdate

##### 调用方式

url=`/tutorupdate`

##### 输入

| Key            | Value 类型 | Value 意义 |
| -------------- | ---------- | ---------- |
| userid         | int        | 用户 ID    |
| subject        | int        | 科目       |
| educationstage | int        | 学段       |
| expprice       | int        | 预期报酬   |
| info           | string     | 备注       |

##### 输出

| Key  | Value 类型 | Value 意义 |
| ---- | ---------- | ---------- |
| -    | -          | -          |

##### 异常处理

UB for now

#### tutorDelete

##### 调用方式

url=`/tutordelete`

##### 输入

| Key            | Value 类型 | Value 意义 |
| -------------- | ---------- | ---------- |
| userid         | int        | 用户 ID    |
| subject        | int        | 科目       |
| educationstage | int        | 学段       |

##### 输出

| Key  | Value 类型 | Value 意义 |
| ---- | ---------- | ---------- |
| -    | -          | -          |

##### 异常处理

UB for now

#### recommend

##### 调用方式

url=`/recommend`

##### 输入

| Key    | Value 类型 | Value 意义 |
| ------ | ---------- | ---------- |
| userid | int        | 用户 ID    |

##### 输出

> [!NOTE]
>
> 数组形式返回

| Key      | Value 类型 | Value 意义      |
| -------- | ---------- | --------------- |
| userid   | int        | 被推荐的用户 ID |
| username | string     | 用户名          |
| sex      | int        | 性别            |
| age      | int        | 年龄            |
| region   | string     | 地区            |
| subject  | int        | 科目            |
| expprice | int        | 预期报酬        |
| rate     | real       | 评分            |
| info     | string     | 备注            |

##### 异常处理

UB for now