-- MariaDB dump 10.19  Distrib 10.11.7-MariaDB, for FreeBSD13.2 (amd64)
--
-- Host: localhost    Database: seproject
-- ------------------------------------------------------
-- Server version	10.11.5-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary table structure for view `matched`
--

DROP TABLE IF EXISTS `matched`;
/*!50001 DROP VIEW IF EXISTS `matched`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `matched` AS SELECT
 1 AS `student`,
  1 AS `teacher`,
  1 AS `subject`,
  1 AS `educationstage` */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `rate`
--

DROP TABLE IF EXISTS `rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rate` (
  `rateeid` int(11) NOT NULL,
  `raterid` int(11) NOT NULL,
  `rate` double NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `info` text DEFAULT NULL,
  PRIMARY KEY (`rateeid`,`raterid`),
  KEY `raterid` (`raterid`),
  CONSTRAINT `rate_ibfk_1` FOREIGN KEY (`rateeid`) REFERENCES `user` (`userid`) ON DELETE CASCADE,
  CONSTRAINT `rate_ibfk_2` FOREIGN KEY (`raterid`) REFERENCES `user` (`userid`) ON DELETE CASCADE,
  CONSTRAINT `rate_chk_1` CHECK (`rate` >= 0 and `rate` <= 10)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rate`
--

LOCK TABLES `rate` WRITE;
/*!40000 ALTER TABLE `rate` DISABLE KEYS */;
/*!40000 ALTER TABLE `rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requirement`
--

DROP TABLE IF EXISTS `requirement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requirement` (
  `userid` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  `educationstage` int(11) NOT NULL,
  `info` text DEFAULT NULL,
  PRIMARY KEY (`userid`,`subject`,`educationstage`),
  CONSTRAINT `requirement_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`userid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requirement`
--

LOCK TABLES `requirement` WRITE;
/*!40000 ALTER TABLE `requirement` DISABLE KEYS */;
/*!40000 ALTER TABLE `requirement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutor`
--

DROP TABLE IF EXISTS `tutor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tutor` (
  `userid` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  `educationstage` int(11) NOT NULL,
  `expprice` int(11) NOT NULL,
  `info` text DEFAULT NULL,
  PRIMARY KEY (`userid`,`subject`,`educationstage`),
  CONSTRAINT `tutor_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`userid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutor`
--

LOCK TABLES `tutor` WRITE;
/*!40000 ALTER TABLE `tutor` DISABLE KEYS */;
/*!40000 ALTER TABLE `tutor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `class` tinyint(4) NOT NULL,
  `sex` tinyint(4) NOT NULL,
  `age` tinyint(4) NOT NULL,
  `region` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `user_rate`
--

DROP TABLE IF EXISTS `user_rate`;
/*!50001 DROP VIEW IF EXISTS `user_rate`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `user_rate` AS SELECT
 1 AS `userid`,
  1 AS `name`,
  1 AS `class`,
  1 AS `sex`,
  1 AS `age`,
  1 AS `region`,
  1 AS `password`,
  1 AS `email`,
  1 AS `rate` */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `matched`
--

/*!50001 DROP VIEW IF EXISTS `matched`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb3 */;
/*!50001 SET character_set_results     = utf8mb3 */;
/*!50001 SET collation_connection      = utf8mb3_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`seproject`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `matched` AS select `r`.`userid` AS `student`,`t`.`userid` AS `teacher`,`r`.`subject` AS `subject`,`r`.`educationstage` AS `educationstage` from (`requirement` `r` join `tutor` `t` on(`r`.`educationstage` = `t`.`educationstage` and `r`.`subject` = `t`.`subject`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `user_rate`
--

/*!50001 DROP VIEW IF EXISTS `user_rate`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb3 */;
/*!50001 SET character_set_results     = utf8mb3 */;
/*!50001 SET collation_connection      = utf8mb3_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`seproject`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `user_rate` AS select `u`.`userid` AS `userid`,`u`.`name` AS `name`,`u`.`class` AS `class`,`u`.`sex` AS `sex`,`u`.`age` AS `age`,`u`.`region` AS `region`,`u`.`password` AS `password`,`u`.`email` AS `email`,avg(`r`.`rate`) AS `rate` from (`user` `u` left join `rate` `r` on(`u`.`userid` = `r`.`rateeid`)) group by `u`.`userid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-06-28 18:38:19
