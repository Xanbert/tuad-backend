package handler

import (
	"database/sql"
	"log"
	"os"
	"os/exec"
	"time"
	"fmt"
	"strings"

	"github.com/go-sql-driver/mysql"
	"github.com/spf13/viper"
)

var db *sql.DB
var dbCfg mysql.Config

func Init() {
	config := viper.New()
	config.SetConfigName("database")
	config.SetConfigType("json")
	config.AddConfigPath("config")
	err := config.ReadInConfig()
	if err == nil {
		log.Print("Using database config from file")
		dbCfg.User = config.GetString("user")
		dbCfg.Passwd = config.GetString("password")
		dbCfg.Net = config.GetString("protocol")
		dbCfg.Addr = config.GetString("address")
		dbCfg.DBName = config.GetString("database")
		dbCfg.AllowNativePasswords = true
	} else {
		log.Print("Unable to get database config from file. Using default value.")
		dbCfg.User = "seproject"
		dbCfg.Passwd = "12345678"
		dbCfg.Net = "tcp"
		dbCfg.Addr = "127.0.0.1:3306"
		dbCfg.DBName = "seproject"
		dbCfg.AllowNativePasswords = true
	}
	var dbErr error
	db, dbErr = sql.Open("mysql", dbCfg.FormatDSN())
	if dbErr != nil {
		log.Fatal(dbErr)
	}
	dbErr = db.Ping()
	if dbErr != nil {
		log.Fatal(dbErr)
	}
}

func DbBackup() (string, error) {
	addr := strings.SplitN(dbCfg.Addr, ":", 2)
	if len(addr) == 1 {
		return "", fmt.Errorf("invalid database address")
	}
	filename := "backup/dump-"
	filename += time.Now().Format("2006-01-02-15-04-05")
	filename += ".sql"
	file, err := os.Create(filename)
	if err != nil {
		return filename, err
	}
	defaultConfigArgs := []string{
		"--add-drop-database",
		"--add-drop-table",
		"--add-locks",
		"--lock-tables",
	}
	cmd := exec.Command("mysqldump" , append(defaultConfigArgs , []string{ "-u" , dbCfg.User , "-h" , addr[0] , "-P" , addr[1], "-p" + dbCfg.Passwd , dbCfg.DBName}...)...)
	cmd.Stdout = file
	err = cmd.Run()
	return filename, err
}

func DbRestore(filename string) error {
	addr := strings.SplitN(dbCfg.Addr, ":", 2)
	if len(addr) == 1 {
		return fmt.Errorf("invalid database address")
	}
	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()
	cmd := exec.Command("mysql", "-u", dbCfg.User, "-h", addr[0], "-P", addr[1], "-p" + dbCfg.Passwd, dbCfg.DBName)
	cmd.Stdin = file
	err = cmd.Run()
	return err
}
