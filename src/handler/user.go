package handler

import (
	"database/sql"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	"backend/src/utils"
)

type DbTableUser struct {
	id       int64
	name     string
	class    int64
	sex      int64
	age      int64
	region   string
	password string
	email    string
}

func Register(c *gin.Context) {
	var err error
	var newEntry DbTableUser
	newEntry.id = 0
	newEntry.name = c.PostForm("username")
	newEntry.class, err = strconv.ParseInt(c.PostForm("class"), 10, 16)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.sex, err = strconv.ParseInt(c.PostForm("sex"), 10, 8)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.age, err = strconv.ParseInt(c.PostForm("age"), 10, 8)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.email = c.PostForm("email")
	newEntry.region = c.PostForm("region")
	newEntry.password = c.PostForm("password")
	result, err := db.Exec("INSERT INTO user (name, class, sex, age, region, password, email) VALUES (?, ?, ?, ?, ?, ?, ?)", newEntry.name, newEntry.class, newEntry.sex, newEntry.age, newEntry.region, newEntry.password, newEntry.email)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.id, err = result.LastInsertId()
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"userid": newEntry.id,
		"class":  newEntry.class,
	})
}

func Login(c *gin.Context) {
	name := c.PostForm("username")
	password := c.PostForm("password")
	row := db.QueryRow("SELECT userid, class FROM user WHERE name = ? AND password = ?", name, password)
	var id, class int64
	err := row.Scan(&id, &class)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"userid": id,
		"class":  class,
	})
}

func UserUpdate(c *gin.Context) {
	var err error
	var user DbTableUser
	user.id, err = strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	user.name = c.PostForm("username")
	user.class, err = strconv.ParseInt(c.PostForm("class"), 10, 16)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	user.sex, err = strconv.ParseInt(c.PostForm("sex"), 10, 8)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	user.age, err = strconv.ParseInt(c.PostForm("age"), 10, 8)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	user.region = c.PostForm("region")
	user.password = c.PostForm("password")
	_, err = db.Exec("UPDATE user SET name = ?, class = ?, sex = ?, age = ?, region = ?, password = ? WHERE userid = ?", user.name, user.class, user.sex, user.age, user.region, user.password, user.id)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"userid": user.id,
		"class":  user.class,
	})
}

func UserDel(c *gin.Context) {
	id, err := strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	_, err = db.Exec("DELETE FROM user WHERE userid = ?", id)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}

func UserInfo(c *gin.Context) {
	var err error
	var user DbTableUser
	user.id, err = strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	row := db.QueryRow("SELECT * FROM user_rate WHERE userid = ?", user.id)
	var rate sql.NullFloat64
	err = row.Scan(&user.id, &user.name, &user.class, &user.sex, &user.age, &user.region, &user.password, &user.email, &rate)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	if rate.Valid {
		c.JSON(http.StatusOK, gin.H{
			"username": user.name,
			"class":    user.class,
			"sex":      user.sex,
			"age":      user.age,
			"region":   user.region,
			"rate":     rate.Float64,
			"email":    user.email,
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"username": user.name,
			"class":    user.class,
			"sex":      user.sex,
			"age":      user.age,
			"region":   user.region,
			"rate":     0.0,
			"email":    user.email,
		})
	}
}

func UserList(c *gin.Context) {
	type JsonDbTableUser struct {
		UserId   int64  `json:"userid"`
		Username string `json:"username"`
		Password string `json:"password"`
		Class    int64  `json:"class"`
		Sex      int64  `json:"sex"`
		Age      int64  `json:"age"`
		Region   string `json:"region"`
		Email    string `json:"email"`
	}
	users := make([]JsonDbTableUser, 0)
	rows, err := db.Query("SELECT * FROM user")
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	defer rows.Close()
	for rows.Next() {
		var user JsonDbTableUser
		err := rows.Scan(&user.UserId, &user.Username, &user.Class, &user.Sex, &user.Age, &user.Region, &user.Password, &user.Email)
		if err != nil {
			utils.LogError(err)
			c.JSON(http.StatusBadRequest, gin.H{})
			return
		}
		users = append(users, user)
	}
	err = rows.Err()
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"array": users,
	})
}
