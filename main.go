package main

import (
	"database/sql"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-sql-driver/mysql"
	"runtime"
)

func logError(err error) {
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		log.Printf("Error: %s\nOccurred in file: %s on line: %d", err, file, line)
	}
}

var db *sql.DB

type DbTableUser struct {
	id       int64
	name     string
	class    int64
	sex      int64
	age      int64
	region   string
	password string
}

type DbTableTutor struct {
	id             int64
	subject        int64
	educationstage int64
	expprice       int64
	rate           float32
	info           string
}

type DbTableRequirement struct {
	id             int64
	subject        int64
	educationstage int64
	info           string
}

func main() {
	dbCfg := mysql.Config{
		User:   "seproject",
		Passwd: "",
		Net:    "tcp",
		Addr:   "127.0.0.1:3306",
		DBName: "seproject",
	}
	var dbErr error
	db, dbErr = sql.Open("mysql", dbCfg.FormatDSN())
	if dbErr != nil {
		log.Fatal(dbErr)
	}
	dbErr = db.Ping()
	if dbErr != nil {
		log.Fatal(dbErr)
	}

	router := gin.Default()
	router.POST("/register", register)
	router.POST("/login", login)
	router.POST("/userupdate", userUpdate)
	router.POST("/userdel", userDel)
	router.POST("/userlist", userList)
	router.POST("/tutoradd", tutorAdd)
	router.POST("/tutorlist", tutorList)
	router.POST("/requirementadd", requirementAdd)

	router.Run(":8080")
}

func register(c *gin.Context) {
	var err error
	var newEntry DbTableUser
	newEntry.id = 0
	newEntry.name = c.PostForm("username")
	newEntry.class, err = strconv.ParseInt(c.PostForm("class"), 10, 16)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.sex, err = strconv.ParseInt(c.PostForm("sex"), 10, 8)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.age, err = strconv.ParseInt(c.PostForm("age"), 10, 8)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.region = c.PostForm("region")
	newEntry.password = c.PostForm("password")
	result, err := db.Exec("INSERT INTO user (name, class, sex, age, region, password) VALUES (?, ?, ?, ?, ?, ?)", newEntry.name, newEntry.class, newEntry.sex, newEntry.age, newEntry.region, newEntry.password)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.id, err = result.LastInsertId()
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"userid": newEntry.id,
		"class":  newEntry.class,
	})
}

func login(c *gin.Context) {
	name := c.PostForm("username")
	password := c.PostForm("password")
	row := db.QueryRow("SELECT userid, class FROM user WHERE name = ? AND password = ?", name, password)
	var id, class int64
	err := row.Scan(&id, &class)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"userid": id,
		"class":  class,
	})
}

func userUpdate(c *gin.Context) {
	var err error
	var user DbTableUser
	user.id, err = strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	user.name = c.PostForm("username")
	user.class, err = strconv.ParseInt(c.PostForm("class"), 10, 16)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	user.sex, err = strconv.ParseInt(c.PostForm("sex"), 10, 8)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	user.age, err = strconv.ParseInt(c.PostForm("age"), 10, 8)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	user.region = c.PostForm("region")
	user.password = c.PostForm("password")
	_, err = db.Exec("UPDATE user SET name = ?, class = ?, sex = ?, age = ?, region = ?, password = ? WHERE userid = ?", user.name, user.class, user.sex, user.age, user.region, user.password, user.id)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"userid": user.id,
		"class":  user.class,
	})
}

func userDel(c *gin.Context) {
	id, err := strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	_, err = db.Exec("DELETE FROM user WHERE userid = ?", id)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}

func userList(c *gin.Context) {
	type JsonDbTableUser struct {
		UserId   int64  `json:"userid"`
		Username string `json:"username"`
		Password string `json:"password"`
		Class    int64  `json:"class"`
		Sex      int64  `json:"sex"`
		Age      int64  `json:"age"`
		Region   string `json:"region"`
	}
	users := make([]JsonDbTableUser, 0)
	rows, err := db.Query("SELECT * FROM user")
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	defer rows.Close()
	for rows.Next() {
		var user JsonDbTableUser
		err := rows.Scan(&user.UserId, &user.Username, &user.Class, &user.Sex, &user.Age, &user.Region, &user.Password)
		if err != nil {
			logError(err)
			c.JSON(http.StatusBadRequest, gin.H{})
			return
		}
		users = append(users, user)
	}
	err = rows.Err()
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"array": users,
	})
}

func tutorAdd(c *gin.Context) {
	var err error
	var newEntry DbTableTutor
	newEntry.id, err = strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.subject, err = strconv.ParseInt(c.PostForm("subject"), 10, 64)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.educationstage, err = strconv.ParseInt(c.PostForm("educationstage"), 10, 64)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.expprice, err = strconv.ParseInt(c.PostForm("expprice"), 10, 64)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.rate = 0.0
	newEntry.info = c.PostForm("info")
	result, err := db.Exec("INSERT INTO tutor (userid, subject, educationstage, expprice, rate, info) VALUES (?, ?, ?, ?, ?, ?)", newEntry.id, newEntry.subject, newEntry.educationstage, newEntry.expprice, newEntry.rate, newEntry.info)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	_, err = result.LastInsertId()
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}

func tutorList(c *gin.Context) {
	type JsonDbTableTutor struct {
		Username string  `json:"username"`
		Sex      int64   `json:"sex"`
		Age      int64   `json:"age"`
		Region   string  `json:"region"`
		Subject  int64   `json:"subject"`
		Expprice int64   `json:"expprice"`
		Rate     float32 `json:"rate"`
		Info     string  `json:"info"`
	}
	type QueryFilter struct {
		subject        int64
		sex            int64
		region         string
		educationstage int64
		lowrate        float32
		lowprice       int64
		highprice      int64
	}
	var err error
	var filter QueryFilter
	query := "SELECT name, sex, age, region, subject, expprice, rate, info FROM user NATURAL JOIN tutor WHERE 0=0 "
	args := []interface{}{}
	filter.subject, err = strconv.ParseInt(c.PostForm("subject"), 10, 64)
	if err == nil {
		query += "AND subject = ? "
		args = append(args, filter.subject)
	}
	filter.sex, err = strconv.ParseInt(c.PostForm("sex"), 10, 8)
	if err == nil {
		query += "AND sex = ? "
		args = append(args, filter.sex)
	}
	filter.region = c.PostForm("region")
	if filter.region != "" {
		query += "AND region = ? "
		args = append(args, filter.region)
	}
	filter.educationstage, err = strconv.ParseInt(c.PostForm("educationstage"), 10, 64)
	if err == nil {
		query += "AND educationstage = ? "
		args = append(args, filter.educationstage)
	}
	f64, _ := strconv.ParseFloat(c.PostForm("lowrate"), 32)
	filter.lowrate = float32(f64)
	filter.lowprice, err = strconv.ParseInt(c.PostForm("lowprice"), 10, 64)
	if err != nil {
		filter.lowprice = 0
	}
	filter.highprice, err = strconv.ParseInt(c.PostForm("highprice"), 10, 64)
	if err == nil {
		query += "AND expprice BETWEEN ? AND ? "
		args = append(args, filter.lowprice, filter.highprice)
	}
	users := make([]JsonDbTableTutor, 0)
	rows, err := db.Query(query, args...)
	if err != nil {
		logError(err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		var user JsonDbTableTutor
		err := rows.Scan(&user.Username, &user.Sex, &user.Age, &user.Region, &user.Subject, &user.Expprice, &user.Rate, &user.Info)
		if err != nil {
			logError(err)
			return
		}
		users = append(users, user)
	}
	err = rows.Err()
	if err != nil {
		logError(err)
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"array": users,
	})
}

func requirementAdd(c *gin.Context) {
	var err error
	var newEntry DbTableRequirement
	newEntry.id, err = strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.subject, err = strconv.ParseInt(c.PostForm("subject"), 10, 64)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.educationstage, err = strconv.ParseInt(c.PostForm("educationstage"), 10, 64)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.info = c.PostForm("info")
	result, err := db.Exec("INSERT INTO requirement (userid, subject, educationstage, info) VALUES (?, ?, ?, ?)", newEntry.id, newEntry.subject, newEntry.educationstage, newEntry.info)
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	_, err = result.LastInsertId()
	if err != nil {
		logError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}
