package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	"backend/src/utils"
)

type DbTableRate struct {
	rateeid int64
	raterid int64
	rate    float32
	info    string
}

func RateAdd(c *gin.Context) {
	var err error
	var newEntry DbTableRate
	newEntry.raterid, err = strconv.ParseInt(c.PostForm("raterid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.rateeid, err = strconv.ParseInt(c.PostForm("rateeid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	f64, err := strconv.ParseFloat(c.PostForm("rate"), 32)
	newEntry.rate = float32(f64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.info = c.PostForm("info")
	result, err := db.Exec("INSERT INTO rate (rateeid, raterid, rate, time, info) VALUES (?, ?, ?, NOW(), ?)", newEntry.rateeid, newEntry.raterid, newEntry.rate, newEntry.info)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	_, err = result.LastInsertId()
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}

func RateUpdate(c *gin.Context) {
	var err error
	var req DbTableRate
	req.raterid, err = strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	req.rateeid, err = strconv.ParseInt(c.PostForm("rateeid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	f64, err := strconv.ParseFloat(c.PostForm("rate"), 32)
	req.rate = float32(f64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	req.info = c.PostForm("info")
	_, err = db.Exec("UPDATE rate SET rate = ?, info = ?, time= NOW() WHERE rateeid = ? AND raterid = ?", req.rate, req.info, req.rateeid, req.raterid)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}

func RateDel(c *gin.Context) {
	cmd := "DELETE FROM rate WHERE raterid = ? "
	args := []interface{}{}
	id, err := strconv.ParseInt(c.PostForm("raterid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	args = append(args, id)
	subject, err := strconv.ParseInt(c.PostForm("rateeid"), 10, 64)
	if err == nil {
		cmd += "AND rateeid = ? "
		args = append(args, subject)
	}
	_, err = db.Exec(cmd, args...)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}

func TutorRate(c *gin.Context) {
	type JsonTutorRate struct {
		Userid int64   `json:"userid"`
		Rate   float32 `json:"rate"`
		Time   string  `json:"time"`
		Info   string  `json:"info"`
	}
	id, err := strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	rates := make([]JsonTutorRate, 0)
	var useless int64 // _ not usable on passing by reference
	rows, err := db.Query("SELECT * FROM rate WHERE rateeid = ?", id)
	if err != nil {
		utils.LogError(err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		var rate JsonTutorRate
		err := rows.Scan(&useless, &rate.Userid, &rate.Rate, &rate.Time, &rate.Info)
		if err != nil {

			utils.LogError(err)
			c.JSON(http.StatusBadRequest, gin.H{})
			return
		}
		rates = append(rates, rate)
	}
	err = rows.Err()
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"array": rates,
	})
}

func UserRate(c *gin.Context) {
	type JsonUserRate struct {
		Userid int64   `json:"userid"`
		Rate   float32 `json:"rate"`
		Time   string  `json:"time"`
		Info   string  `json:"info"`
	}
	id, err := strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	rates := make([]JsonUserRate, 0)
	var useless int64 // _ not usable on passing by reference
	rows, err := db.Query("SELECT * FROM rate WHERE raterid = ?", id)
	if err != nil {
		utils.LogError(err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		var rate JsonUserRate
		err := rows.Scan(&rate.Userid, &useless, &rate.Rate, &rate.Time, &rate.Info)
		if err != nil {
			utils.LogError(err)
			c.JSON(http.StatusBadRequest, gin.H{})
			return
		}
		rates = append(rates, rate)
	}
	err = rows.Err()
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"array": rates,
	})
}
