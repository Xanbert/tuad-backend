package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	"backend/src/utils"
)

type DbTableRequirement struct {
	id             int64
	subject        int64
	educationstage int64
	info           string
}

func RequirementAdd(c *gin.Context) {
	var err error
	var newEntry DbTableRequirement
	newEntry.id, err = strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.subject, err = strconv.ParseInt(c.PostForm("subject"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.educationstage, err = strconv.ParseInt(c.PostForm("educationstage"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.info = c.PostForm("info")
	result, err := db.Exec("INSERT INTO requirement (userid, subject, educationstage, info) VALUES (?, ?, ?, ?)", newEntry.id, newEntry.subject, newEntry.educationstage, newEntry.info)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	_, err = result.LastInsertId()
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}

func RequirementUpdate(c *gin.Context) {
	var err error
	var req DbTableTutor
	req.id, err = strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	req.subject, err = strconv.ParseInt(c.PostForm("subject"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	req.educationstage, err = strconv.ParseInt(c.PostForm("educationstage"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	req.info = c.PostForm("info")
	_, err = db.Exec("UPDATE requirement SET info = ? WHERE userid = ? AND subject = ? AND educationstage = ?", req.info, req.id, req.subject, req.educationstage)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}

func RequirementDel(c *gin.Context) {
	cmd := "DELETE FROM requirement WHERE userid = ? "
	args := []interface{}{}
	id, err := strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	args = append(args, id)
	subject, err := strconv.ParseInt(c.PostForm("subject"), 10, 64)
	if err == nil {
		cmd += "AND subject = ? "
		args = append(args, subject)
	}
	educationstage, err := strconv.ParseInt(c.PostForm("educationstage"), 10, 64)
	if err == nil {
		cmd += "AND educationstage = ? "
		args = append(args, educationstage)
	}
	_, err = db.Exec(cmd, args...)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}

func RequirementInfo(c *gin.Context) {
	type JsonTutorInfo struct {
		Subject        int64  `json:"subject"`
		Educationstage int64  `json:"educationstage"`
		Info           string `json:"info"`
	}
	id, err := strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	info := make([]JsonTutorInfo, 0)
	var useless int64 // _ not usable on passing by reference
	rows, err := db.Query("SELECT * FROM requirement WHERE userid = ?", id)
	if err != nil {
		utils.LogError(err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		var tutor JsonTutorInfo
		err := rows.Scan(&useless, &tutor.Subject, &tutor.Educationstage, &tutor.Info)
		if err != nil {
			utils.LogError(err)
			c.JSON(http.StatusBadRequest, gin.H{})
			return
		}
		info = append(info, tutor)
	}
	err = rows.Err()
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"array": info,
	})
}
