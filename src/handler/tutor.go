package handler

import (
	"database/sql"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	"backend/src/utils"
)

type DbTableTutor struct {
	id             int64
	subject        int64
	educationstage int64
	expprice       int64
	info           string
}

func TutorAdd(c *gin.Context) {
	var err error
	var newEntry DbTableTutor
	newEntry.id, err = strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.subject, err = strconv.ParseInt(c.PostForm("subject"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.educationstage, err = strconv.ParseInt(c.PostForm("educationstage"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.expprice, err = strconv.ParseInt(c.PostForm("expprice"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	newEntry.info = c.PostForm("info")
	result, err := db.Exec("INSERT INTO tutor (userid, subject, educationstage, expprice, info) VALUES (?, ?, ?, ?, ?)", newEntry.id, newEntry.subject, newEntry.educationstage, newEntry.expprice, newEntry.info)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	_, err = result.LastInsertId()
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}

func TutorUpdate(c *gin.Context) {
	var err error
	var tutor DbTableTutor
	tutor.id, err = strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	tutor.subject, err = strconv.ParseInt(c.PostForm("subject"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	tutor.educationstage, err = strconv.ParseInt(c.PostForm("educationstage"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	tutor.expprice, err = strconv.ParseInt(c.PostForm("expprice"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	tutor.info = c.PostForm("info")
	_, err = db.Exec("UPDATE tutor SET expprice = ?, info = ? WHERE userid = ? AND subject = ? AND educationstage = ?", tutor.expprice, tutor.info, tutor.id, tutor.subject, tutor.educationstage)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}

func TutorDel(c *gin.Context) {
	cmd := "DELETE FROM tutor WHERE userid = ? "
	args := []interface{}{}
	id, err := strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	args = append(args, id)
	subject, err := strconv.ParseInt(c.PostForm("subject"), 10, 64)
	if err == nil {
		cmd += "AND subject = ? "
		args = append(args, subject)
	}
	educationstage, err := strconv.ParseInt(c.PostForm("educationstage"), 10, 64)
	if err == nil {
		cmd += "AND educationstage = ? "
		args = append(args, educationstage)
	}
	_, err = db.Exec(cmd, args...)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}

func TutorInfo(c *gin.Context) {
	type JsonTutorInfo struct {
		Subject        int64   `json:"subject"`
		Educationstage int64   `json:"educationstage"`
		Expprice       int64   `json:"expprice"`
		Rate           float32 `json:"rate"`
		Info           string  `json:"info"`
	}
	id, err := strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	info := make([]JsonTutorInfo, 0)
	var useless int64 // _ not usable on passing by reference
	rows, err := db.Query("SELECT * FROM tutor NATUAL JOIN user_rate WHERE userid = ?", id)
	if err != nil {
		utils.LogError(err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		var tutor JsonTutorInfo
		var rate sql.NullFloat64
		err := rows.Scan(&useless, &tutor.Subject, &tutor.Educationstage, &tutor.Expprice, &rate, &tutor.Info)
		if rate.Valid {
			tutor.Rate = float32(rate.Float64)
		} else {
			tutor.Rate = 0
		}
		if err != nil {
			utils.LogError(err)
			c.JSON(http.StatusBadRequest, gin.H{})
			return
		}
		info = append(info, tutor)
	}
	err = rows.Err()
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"array": info,
	})
}

func TutorList(c *gin.Context) {
	type JsonDbTableTutor struct {
		Userid         int64   `json:"userid"`
		Username       string  `json:"username"`
		Sex            int64   `json:"sex"`
		Age            int64   `json:"age"`
		Region         string  `json:"region"`
		Subject        int64   `json:"subject"`
		Expprice       int64   `json:"expprice"`
		Rate           float32 `json:"rate"`
		Info           string  `json:"info"`
		Educationstage int64   `json:"EducationStage"`
		Email          string  `json:"email"`
	}
	type QueryFilter struct {
		subject        int64
		sex            int64
		region         string
		educationstage int64
		lowrate        float32
		lowprice       int64
		highprice      int64
	}
	var err error
	var filter QueryFilter
	query := "SELECT userid, name, sex, age, region, subject, expprice, rate, info, educationstage, email FROM user_rate NATURAL JOIN tutor WHERE 0=0 "
	args := []interface{}{}
	filter.subject, err = strconv.ParseInt(c.PostForm("subject"), 10, 64)
	if err == nil {
		query += "AND subject = ? "
		args = append(args, filter.subject)
	}
	filter.sex, err = strconv.ParseInt(c.PostForm("sex"), 10, 8)
	if err == nil {
		query += "AND sex = ? "
		args = append(args, filter.sex)
	}
	filter.region = c.PostForm("region")
	if filter.region != "" {
		query += "AND region = ? "
		args = append(args, filter.region)
	}
	filter.educationstage, err = strconv.ParseInt(c.PostForm("educationstage"), 10, 64)
	if err == nil {
		query += "AND educationstage = ? "
		args = append(args, filter.educationstage)
	}
	f64, err := strconv.ParseFloat(c.PostForm("lowrate"), 32)
	filter.lowrate = float32(f64)
	if err == nil && filter.lowrate > 0.01 {
		query += "AND rate >= ? "
		args = append(args, filter.lowrate)
	}
	filter.lowprice, err = strconv.ParseInt(c.PostForm("lowprice"), 10, 64)
	if err == nil {
		query += "AND expprice >= ? "
		args = append(args, filter.lowprice)
	}
	filter.highprice, err = strconv.ParseInt(c.PostForm("highprice"), 10, 64)
	if err == nil {
		query += "AND expprice <= ? "
		args = append(args, filter.highprice)
	}
	users := make([]JsonDbTableTutor, 0)
	rows, err := db.Query(query, args...)
	if err != nil {
		utils.LogError(err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		var user JsonDbTableTutor
		var rate sql.NullFloat64
		err := rows.Scan(&user.Userid, &user.Username, &user.Sex, &user.Age, &user.Region, &user.Subject, &user.Expprice, &rate, &user.Info, &user.Educationstage, &user.Email)
		if rate.Valid {
			user.Rate = float32(rate.Float64)
		} else {
			user.Rate = 0
		}
		if err != nil {
			utils.LogError(err)
			c.JSON(http.StatusBadRequest, gin.H{})
			return
		}
		users = append(users, user)
	}
	err = rows.Err()
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"array": users,
	})
}
