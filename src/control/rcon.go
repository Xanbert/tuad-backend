package control

import (
	"log"
	"os"
	"strings"

	mcnet "github.com/Tnze/go-mc/net"
	"github.com/spf13/viper"

	"backend/src/handler"
	"backend/src/utils"
)

var password string

func Manager() {
	config := viper.New()
	config.SetConfigName("rcon")
	config.SetConfigType("json")
	config.AddConfigPath("config")
	err := config.ReadInConfig()
	var address string
	if err == nil {
		log.Print("Using rcon config from file")
		address = config.GetString("address")
		password = config.GetString("password")
	} else {
		log.Print("Unable to get rcon config from file. Using default value.")
		address = "localhost:25565"
		password = ""
	}
	listen, err := mcnet.ListenRCON(address)
	if err != nil {
		utils.LogError(err)
		return
	}
	defer listen.Close()

	for {
		conn, err := listen.Accept()
		if err != nil {
			utils.LogError(err)
			return
		}
		go managerHandler(conn)
	}
}

func managerHandler(conn mcnet.RCONServerConn) {
	err := conn.AcceptLogin(password)
	if err != nil {
		log.Print("Invalid login request on rcon connection.")
		conn.Close()
		return
	} else {
		log.Print("Rcon connection established.")
	}
	cmd, err := conn.AcceptCmd()
	var resp, servCmd string
	if err == nil {
		resp, servCmd = handleCmd(cmd)
		err := conn.RespCmd(resp)
		if err != nil {
			log.Print("Error sending command response.")
		}
	} else {
		log.Print("Error accepting command.")
	}
	for failCount := 0; strings.Compare(servCmd, "exit") != 0; {
		cmd, err = conn.AcceptCmd()
		if err == nil {
			failCount = 0
			resp, servCmd = handleCmd(cmd)
			err := conn.RespCmd(resp)
			if err != nil {
				log.Print("Error sending command response.")
			}
		} else {
			log.Print("Error accepting command.")
			failCount += 1
			if failCount > 3 {
				log.Print("Too many failure while receiving command. Closing connection.")
				break
			}
		}
		if strings.Compare(servCmd, "stop") == 0 {
			conn.Close()
			os.Exit(0)
		}
	}
	log.Print("Rcon connection closed.")
	conn.Close()
}

func handleCmd(cmd string) (string, string) {
	cmdlist := strings.SplitN(cmd, " ", 2)
	if len(cmdlist) == 1 {
		cmdlist = append(cmdlist, "")
	}
	var resp string
	switch cmdlist[0] {
	case "exit":
		resp = "Bye"
	case "stop":
		log.Print("Rcon called for a shutdown.")
		resp = "Shutting down the server."
	case "backup":
		log.Printf("Rcon called for database backup.")
		filename, err := handler.DbBackup()
		if err == nil {
			resp = "Backup created as " + filename + "."
		} else {
			utils.LogError(err)
			resp = "Unable to create backup. See log for more info."
		}
	case "restore":
		log.Printf("Rcon called for database restore.")
		err := handler.DbRestore(cmdlist[1])
		if err == nil {
			resp = "Restored from " + cmdlist[1] + "."
		} else {
			utils.LogError(err)
			resp = "Unable to restore. See log for more info."
		}
	default:
		resp = "Invalid command."
	}
	return resp, cmdlist[0]
}
