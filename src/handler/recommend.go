package handler

import (
	"database/sql"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	"backend/src/utils"
)

func Recommend(c *gin.Context) {
	type JsonRecommendTeacher struct {
		Userid         int64   `json:"userid"`
		Username       string  `json:"username"`
		Sex            int64   `json:"sex"`
		Age            int64   `json:"age"`
		Region         string  `json:"region"`
		Subject        int64   `json:"subject"`
		Educationstage int64   `json:"educationstage"`
		Expprice       int64   `json:"expprice"`
		Rate           float32 `json:"rate"`
		Info           string  `json:"info"`
		Email          string  `json:"email"`
	}
	type JsonRecommendStudent struct {
		Userid         int64  `json:"userid"`
		Username       string `json:"username"`
		Sex            int64  `json:"sex"`
		Age            int64  `json:"age"`
		Region         string `json:"region"`
		Subject        int64  `json:"subject"`
		Educationstage int64  `json:"educationstage"`
		Info           string `json:"info"`
		Email          string `json:"email"`
	}
	id, err := strconv.ParseInt(c.PostForm("userid"), 10, 64)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}

	rows, err := db.Query("SELECT class FROM user WHERE userid = ?", id)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}
	var class int
	rows.Next()
	err = rows.Scan(&class)
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}

	switch class {
	case 0: // class student
		rows, err = db.Query("select t.userid, name, sex, age, region, m.subject, m.educationstage, expprice, rate, info, email from matched as m join tutor as t join user_rate as u on m.teacher = t.userid and m.educationstage = t.educationstage and m.subject = t.subject and u.userid=t.userid where student = ?;", id)
	case 1:
		rows, err = db.Query("select r.userid, name, sex, age, region, m.subject, m.educationstage, info, email from matched as m join requirement as r join user_rate as u on m.student = r.userid and m.educationstage=r.educationstage and m.subject=r.subject and u.userid=r.userid where teacher = ?;", id)
	}
	defer rows.Close()
	if err != nil {
		utils.LogError(err)
		c.JSON(http.StatusBadRequest, gin.H{})
		return
	}

	switch class {
	case 0: // class student
		recommendations := make([]JsonRecommendTeacher, 0)
		for rows.Next() {
			var recommendation JsonRecommendTeacher
			var rate sql.NullFloat64
			err := rows.Scan(&recommendation.Userid, &recommendation.Username, &recommendation.Sex, &recommendation.Age, &recommendation.Region, &recommendation.Subject, &recommendation.Educationstage, &recommendation.Expprice, &rate, &recommendation.Info, &recommendation.Email)
			if rate.Valid {
				recommendation.Rate = float32(rate.Float64)
			} else {
				recommendation.Rate = 0
			}
			if err != nil {
				utils.LogError(err)
				c.JSON(http.StatusBadRequest, gin.H{})
				return
			}
			recommendations = append(recommendations, recommendation)
			err = rows.Err()
			if err != nil {
				utils.LogError(err)
				c.JSON(http.StatusBadRequest, gin.H{})
				return
			}
		}
		c.JSON(http.StatusOK, gin.H{
			"array": recommendations,
		})
	case 1: // class teacher
		recommendations := make([]JsonRecommendStudent, 0)
		for rows.Next() {
			var recommendation JsonRecommendStudent
			err := rows.Scan(&recommendation.Userid, &recommendation.Username, &recommendation.Sex, &recommendation.Age, &recommendation.Region, &recommendation.Subject, &recommendation.Educationstage, &recommendation.Info, &recommendation.Email)
			if err != nil {
				utils.LogError(err)
				c.JSON(http.StatusBadRequest, gin.H{})
				return
			}
			recommendations = append(recommendations, recommendation)
			err = rows.Err()
			if err != nil {
				utils.LogError(err)
				c.JSON(http.StatusBadRequest, gin.H{})
				return
			}
		}
		c.JSON(http.StatusOK, gin.H{
			"array": recommendations,
		})
	}
}
