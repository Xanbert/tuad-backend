package main

import (
	"github.com/gin-gonic/gin"

	"backend/src/control"
	"backend/src/handler"
)

func main() {
	handler.Init()

	router := gin.Default()
	router.POST("/register", handler.Register)
	router.POST("/login", handler.Login)
	router.POST("/userupdate", handler.UserUpdate)
	router.POST("/userdel", handler.UserDel)
	router.POST("/userinfo", handler.UserInfo)
	router.POST("/userlist", handler.UserList)
	router.POST("/tutoradd", handler.TutorAdd)
	router.POST("/tutorupdate", handler.TutorUpdate)
	router.POST("/tutordel", handler.TutorDel)
	router.POST("/tutorinfo", handler.TutorInfo)
	router.POST("/tutorlist", handler.TutorList)
	router.POST("/requirementadd", handler.RequirementAdd)
	router.POST("/requirementupdate", handler.RequirementUpdate)
	router.POST("/requirementdel", handler.RequirementDel)
	router.POST("/requirementinfo", handler.RequirementInfo)
	router.POST("/rateadd", handler.RateAdd)
	router.POST("/rateupdate", handler.RateUpdate)
	router.POST("/ratedel", handler.RateDel)
	router.POST("/tutorrate", handler.TutorRate)
	router.POST("/userrate", handler.UserRate)
	router.POST("/recommend", handler.Recommend)

	go control.Manager()
	router.Run(":8080")
}
